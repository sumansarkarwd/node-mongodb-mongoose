const events = require('events');
const genThumbHandler = require('./genThumb.handler');
const someEventHandler = require('./someEvent.handler');

const eventEmitter = new events.EventEmitter();

eventEmitter.on('someEvent', someEventHandler);
eventEmitter.on('genThumbnail', genThumbHandler);

module.exports = eventEmitter;
