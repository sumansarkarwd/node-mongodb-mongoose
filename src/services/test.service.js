const eventEmitter = require('../events/index');

module.exports.test = async () => {
  eventEmitter.emit('someEvent', 'Foo');
  return 'This is purely for testing purpose';
};

module.exports.testPost = async (data) => data;
