const bcrypt = require('bcrypt');
const AlreadyExistsError = require('../errors/alreadyExists.error');
const UnauthorizedError = require('../errors/unauthorized.error');
const { UserModel } = require('../models');
const config = require('../utils/config');

module.exports.register = async (data) => {
  const userExists = await UserModel.findOne({ email: data.email });
  if (userExists) throw new AlreadyExistsError('User');

  data.password = await bcrypt.hash(data.password, config.SALT_ROUNDS);
  const user = await UserModel.create(data);

  return user;
};

module.exports.login = async (data) => {
  const userExists = await UserModel.findOne({ email: data.email });
  if (!userExists) throw new UnauthorizedError();

  const isValid = await bcrypt.compare(data.password, userExists.password);
  if (!isValid) throw new UnauthorizedError();

  return userExists;
};
