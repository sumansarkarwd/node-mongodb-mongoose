const Joi = require('joi');
const _ = require('lodash');
const ValidationError = require('../errors/validation.error');

const validate = (schema) => (req, res, next) => {
  const validSchema = _.pick(schema, ['params', 'query', 'body']);
  const object = _.pick(req, Object.keys(validSchema));

  Joi.compile(validSchema)
    .prefs({ errors: { label: 'key' } })
    .validateAsync(object, {
      errors: {
        wrap: {
          label: '',
        },
      },
    })
    .then(({ value }) => {
      Object.assign(req, value);
      return next();
    })
    .catch((error) => {
      next(new ValidationError(error.message));
    });
};

module.exports = validate;
