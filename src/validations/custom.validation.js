module.exports.password = (value, helpers) => {
  if (value.length < 6) {
    return helpers.message('{{#label}} must be at least 6 characters');
  }
  if (!value.match(/\d/) || !value.match(/[a-zA-Z]/)) {
    return helpers.message('{{#label}} must contain at least 1 letter and 1 number');
  }
  return value;
};

module.exports.mongoId = (value) =>
  new Promise((res, reg) => {
    if (!value.match(/^[0-9a-fA-F]{24}$/)) {
      reg(new Error('Invalid mongoId'));
    } else {
      res(value);
    }
  });
