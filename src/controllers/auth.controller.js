const authService = require('../services/auth.service');
const tokenService = require('../services/token.service');
const SuccessResponse = require('../utils/successResponse');

/**
 * Register
 * @typedef {object} Register
 * @property {string} firstName.form.required
 * @property {string} lastName.form.required
 * @property {string} email.form.required
 * @property {string} password.form.required
 */
/**
 * POST /auth/register
 * @summary Auth
 * @tags Auth
 * @param {Register}  request.body.required
 * @returns {object} - 200 - success response
 * @example response - 200
{
  "data": {
    "user": {
      "firstName": "john",
      "lastName": "doe",
      "email": "john@doe.co",
      "_id": "624a9547843bc70dfab8c6f9",
      "createdAt": "2022-04-04T06:50:47.632Z",
      "updatedAt": "2022-04-04T06:50:47.632Z",
      "id": "624a9547843bc70dfab8c6f9"
    }
  },
  "message": "OK",
  "httpCode": 200
}
*/
module.exports.register = async (req) => {
  const user = await authService.register(req.body);

  return new SuccessResponse({ user });
};

/**
 * Login
 * @typedef {object} Login
 * @property {string} email.form.required
 * @property {string} password.form.required
 */
/**
 * POST /auth/login
 * @summary Auth
 * @tags Auth
 * @param {Login}  request.body.required
 * @returns {object} - 200 - success response
 * @example response - 200
{
  "data": {
    "user": {
      "_id": "624a9547843bc70dfab8c6f9",
      "firstName": "string",
      "lastName": "string",
      "email": "...user-email",
      "createdAt": "2022-04-04T06:50:47.632Z",
      "updatedAt": "2022-04-04T06:50:47.632Z",
      "id": "624a9547843bc70dfab8c6f9"
    },
    "tokens": {
      "access": {
        "token": "...token",
        "expires": "2022-05-01T14:27:29.796Z"
      },
      "refresh": {
        "token": "...token",
        "expires": "2022-05-01T14:27:29.796Z"
      }
    }
  },
  "message": "OK",
  "httpCode": 200
}
*/
module.exports.login = async (req) => {
  const user = await authService.login(req.body);

  const tokens = tokenService.genToken(user);

  return new SuccessResponse({ user, tokens });
};

/**
 * GET /auth/me
 * @summary Auth
 * @tags Auth
 * @security BearerAuth
 * @param {Login}  request.body.required
 * @returns {object} - 200 - success response
 * @example response - 200
{
  "data": {
    "user": {
      "_id": "624a9528366c2f85970a05f5",
      "firstName": "john",
      "lastName": "doe",
      "email": "john@doe.com",
      "createdAt": "2022-04-04T06:50:16.498Z",
      "updatedAt": "2022-04-04T06:50:16.498Z",
      "id": "624a9528366c2f85970a05f5"
    }
  },
  "message": "OK",
  "httpCode": 200
}
*/
module.exports.me = async (req) => {
  const { user } = req;
  return new SuccessResponse({ user });
};
