const testService = require('../services/test.service');
const SuccessResponse = require('../utils/successResponse');

/**
 * GET /test
 * @summary Test
 * @tags Test
 * @returns {object} - 200 - success response
 * @example response - 200
{
  "data": {
    "greeting": "This is purely for testing purpose"
  },
  "message": "OK",
  "httpCode": 200
}
 * @returns {object} - 404 - error response
 * @example response - 404
{
  "message": "Not found",
  "httpCode": 404,
  "errorCode": "NOT_FOUND"
}
 */
module.exports.test = async () => {
  const data = await testService.test();

  return new SuccessResponse({ greeting: data });
};

/**
 * TestPost
 * @typedef {object} TestPost
 * @property {string} password.form.required
 * @property {string} startDate.form.required - YYYY-MM-DD
 * @property {string} endDate.form.required - YYYY-MM-DD
 */
/**
 * POST /test/{id}
 * @summary Test Post
 * @tags Test
 * @param {string} id.path.required - id
 * @param {TestPost}  request.body.required
 * @returns {object} - 200 - success response
 * @example response - 200
{
  "data": {
    "greeting": "This is purely for testing purpose"
  },
  "message": "OK",
  "httpCode": 200
}
 * @returns {object} - 404 - error response
 * @example response - 404
{
  "message": "Not found",
  "httpCode": 404,
  "errorCode": "NOT_FOUND"
}
 */
module.exports.testPost = async (req) => {
  const data = await testService.testPost(req.body);

  return new SuccessResponse({ greeting: data });
};
