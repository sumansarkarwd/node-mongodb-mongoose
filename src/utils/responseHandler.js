module.exports = (func) => async (req, res, next) => {
  try {
    const resp = await func.call(this, req, res, next);
    res.status(resp.httpCode).send(resp);
  } catch (e) {
    next(e);
  }
};
