const { randomUUID } = require('crypto');

module.exports = (name, ext, part) => `${randomUUID()}.${part.mimetype.split('/')[1]}`;
