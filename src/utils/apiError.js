const httpStatus = require('http-status');

class ApiError extends Error {
  constructor(message, errorCode = 'UNKNOWN_ERROR', stack = null, httpCode = httpStatus.INTERNAL_SERVER_ERROR) {
    super(message);
    this.httpCode = httpCode;
    this.errorCode = errorCode;
    if (stack) {
      this.stack = stack;
    } else {
      Error.captureStackTrace(this, this.constructor);
    }
  }
}

module.exports = ApiError;
