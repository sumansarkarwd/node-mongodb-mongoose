const chalk = require('chalk');
const mongoose = require('mongoose');
const config = require('./config');
const logger = require('./logger');

const connectionString = `mongodb://${config.DB_USER}:${config.DB_PASSWORD}@${config.DB_HOST}:${config.DB_PORT}/${config.DB_NAME}?authSource=admin`;

module.exports = mongoose
  .connect(connectionString)
  .then(() => {
    logger.info(chalk.green('DB Connection has been established successfully.'));
  })
  .catch((err) => {
    logger.error(`Unable to connect to the database: ${err?.message}`);
    process.exit(1);
  });
