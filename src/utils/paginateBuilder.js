module.exports = (oldProp, newProp, { [oldProp]: old, ...others }) => ({
  [newProp]: old,
  pagination: others,
});
