/* eslint-disable no-plusplus */
module.exports.asyncForEach = async (array, callback) => {
  const data = {};
  for (let index = 0; index < array.length; index++) {
    data[index] = await callback(array[index], index, array);
  }
  return data;
};
