/* eslint-disable */

/**
 *
 * @param {object} obj1 - body form network req
 * @param {*} obj2 - mongoose find object
 * @returns {boolean} true if both are different else false
 */
const compareArray = (arr1, arr2) => {
  let flag = false;
  for (let i = 0; i < arr1.length; i++) {
    if (flag) break;
    if (Array.isArray(arr1[i])) {
      flag = compareArray(arr1[i], arr2[i]);
    } else if (typeof arr1[i] !== 'object' || !arr1[i]) {
      if (String(arr1[i]) !== String(arr2[i])) flag = true;
    } else {
      flag = compare(arr1[i], arr2[i]);
    }
  }
  return flag;
};

const compare = (obj1, obj2) => {
  const keys = Object.keys(obj1);
  let flag = false;
  for (let i = 0; i < keys.length; i++) {
    if (flag) break;
    if (Array.isArray(obj1[keys[i]])) {
      flag = compareArray(obj1[keys[i]], obj2[keys[i]]);
    } else if (typeof obj1[keys[i]] !== 'object' || !obj1[keys[i]]) {
      if (String(obj1[keys[i]]) !== String(obj2[keys[i]])) {
        flag = true;
      }
    } else flag = compare(obj1[keys[i]], obj2[keys[i]]);
  }

  return flag;
};

module.exports = compare;
