const httpStatus = require('http-status');

module.exports = class SuccessResponse {
  constructor(data, message = 'OK', httpCode = httpStatus.OK) {
    this.data = data;
    this.message = message;
    this.httpCode = httpCode;
  }
};
