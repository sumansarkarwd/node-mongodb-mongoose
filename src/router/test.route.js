const testController = require('../controllers/test.controller.js');
const validate = require('../middlewares/validateData.js');
const mapRoutes = require('../utils/mapRoutes.js');
const { validateTest } = require('../validations/test.validation.js');

const routes = {
  prefix: '/',
  routes: [
    {
      method: 'get',
      route: 'test',
      handler: testController.test,
      middleware: [],
    },
    {
      method: 'post',
      route: 'test/:id',
      handler: testController.testPost,
      middleware: [validate(validateTest)],
    },
  ],
};

module.exports = mapRoutes(routes);
