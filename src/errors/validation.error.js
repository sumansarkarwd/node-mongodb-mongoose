const httpStatus = require('http-status');
const ApiError = require('../utils/apiError');

module.exports = class ValidationError extends ApiError {
  constructor(msg) {
    super(msg, 'VALIDATION_ERROR', null, httpStatus.UNPROCESSABLE_ENTITY);
  }
};
