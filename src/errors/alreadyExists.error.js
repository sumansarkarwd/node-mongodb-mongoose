const httpStatus = require('http-status');
const ApiError = require('../utils/apiError');
const { alreadyExists } = require('../utils/msgFactory');

module.exports = class AlreadyExistsError extends ApiError {
  constructor(entity) {
    super(alreadyExists(entity || 'Item'), 'CONFLICT', null, httpStatus.CONFLICT);
  }
};
