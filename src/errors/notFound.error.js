const httpStatus = require('http-status');
const ApiError = require('../utils/apiError');
const { notFound } = require('../utils/msgFactory');

module.exports = class NotFoundError extends ApiError {
  constructor(entity) {
    super(notFound(entity || 'Item'), 'NOT_FOUND', null, httpStatus.NOT_FOUND);
  }
};
