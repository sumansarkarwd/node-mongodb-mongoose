module.exports.toJSON = require('./toJSON.plugin');
module.exports.aggregatePaginate = require('mongoose-aggregate-paginate-v2');
module.exports.paginate = require('mongoose-paginate-v2');
module.exports.softDelete = require('mongoose-delete');
module.exports.autoIncrement = require('mongoose-auto-increment');
module.exports.autoPopulate = require('mongoose-autopopulate');
