const mongoose = require('mongoose');
const { toJSON, aggregatePaginate, softDelete, autoPopulate, paginate } = require('./plugins');

const schema = mongoose.Schema(
  {
    firstName: {
      type: String,
      trim: true,
    },
    lastName: {
      type: String,
      trim: true,
    },
    email: {
      type: String,
      trim: true,
      lowercase: true,
    },
    password: {
      type: String,
      trim: true,
      private: true,
    },
  },
  {
    timestamps: true,
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  },
);

schema.plugin(toJSON);
schema.plugin(softDelete, {
  overrideMethods: ['count', 'countDocuments', 'findOne', 'findOneAndUpdate', 'update', 'updateOne', 'updateMany'],
  deletedAt: true,
});
schema.plugin(aggregatePaginate);
schema.plugin(autoPopulate);
schema.plugin(paginate);

const model = mongoose.model('user', schema, 'users');

module.exports = model;
