const express = require('express');
const responseHandler = require('../utils/responseHandler');
const config = require('../utils/config');

const testRoute = require('./test.route');
const uploadRoute = require('./upload.route');
const authRoute = require('./auth.route');

const routes = [].concat(testRoute, uploadRoute, authRoute);

const router = express.Router();

routes.forEach((r) => {
  const middlewares = Array.isArray(r.middleware) ? r.middleware : [];
  router[r.method](r.route, ...middlewares, responseHandler(r.handler));
});

router.get('', (req, res) => {
  res.send({
    // eslint-disable-next-line no-useless-escape
    // eslint-disable-next-line no-irregular-whitespace
    message: `Welcome to ${config.APP_NAME} ¯\\_( ͡❛ ͜ʖ ͡❛)_//¯`,
  });
});

module.exports = router;
