const authController = require('../controllers/auth.controller');
const validate = require('../middlewares/validateData');
const auth = require('../middlewares/auth.middleware');

const mapRoutes = require('../utils/mapRoutes');
const { register, login } = require('../validations/auth.validation');

const routes = {
  prefix: '/auth',
  routes: [
    {
      method: 'post',
      route: '/register',
      handler: authController.register,
      middleware: [validate(register)],
    },
    {
      method: 'post',
      route: '/login',
      handler: authController.login,
      middleware: [validate(login)],
    },
    {
      method: 'get',
      route: '/me',
      handler: authController.me,
      middleware: [auth()],
    },
  ],
};

module.exports = mapRoutes(routes);
