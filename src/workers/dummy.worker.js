const { parentPort } = require('worker_threads');
const config = require('../utils/config');
const logger = require('../utils/logger');

parentPort.postMessage('Started doing my job!');

logger.info(`Doing some heavy work... ${config.APP_NAME}`);

parentPort.postMessage('Done doing my job!');
